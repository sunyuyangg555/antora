= Index Page
:description: The almighty index page

Content.

See xref:the-page.adoc[the page], which you can also access via xref:the-alias.adoc[its alias].

Also check out xref:the-freshness#[the new page].

include::{partialsdir}/diagrams.adoc[]
