= Run Antora in a Container
// URLs
:url-docs-job: https://gitlab.com/antora/docs.antora.org/blob/master/.gitlab-ci.yml
:url-docker: https://docs.docker.com
:url-podman: https://podman.io
:url-docker-hub: https://hub.docker.com/r/antora/antora
:url-plantuml-npm: https://www.npmjs.com/package/asciidoctor-plantuml

The Antora project provides a Docker image that you can use to run the `antora` command inside a container (a process known as [.term]*containerization*).
The benefit of this approach is that you can bypass installing Antora and get right down to running it.
All you need is Docker or podman.

Assumptions:

* [x] You have {url-docker}[Docker] (command: `docker`) or {url-podman}[podman] (command: `podman`) installed on your machine.
* [x] The Docker daemon is running on your machine (not required when using podman).
* [x] You have configured your own xref:playbook:index.adoc[playbook] or you're using the Demo playbook.

On this page, you'll learn:

* [x] How to run Antora inside a container using the official Docker image for Antora.
* [x] How to give the container access to a local directory.
* [x] How to extend the Docker image for Antora to create your own image.

== Docker image for Antora

{url-docker}[Docker] is a tool for running container images (officially OCI images).
You can think of a container image as an application in a box.
Inside that box is everything you need to run the application, including the code, the runtime, the settings, and even the operating system itself.
Containers not only isolate software from the host environment, they also make it easy to get up and running quickly.
And that's a perfect way to discover and explore Antora!

The Antora project provides an official Docker (OCI) image named `antora/antora` for running Antora inside a container.
This image is published to the {url-docker-hub}[antora/antora project^] on Docker Hub.

This image is a drop-in replacement for the `antora` command.
Rather than installing the `antora` command on your own computer or in a CI environment, you simply run the command by running the container.
In fact, the {url-docs-job}[CI job for the Antora documentation site] uses this image to generate the documentation you're currently reading.

Let's find out how to run it.

== Run the Antora image

To demonstrate how to use this image, we'll be using the Antora demo site.
Start by cloning the playbook repository for the demo site, then switch to the newly created folder:

 ~ $ git clone https://gitlab.com/antora/demo/docs-site.git && cd "$(basename $_ .git)"

Next, execute the `docker run` command to invoke the entrypoint command (i.e., `antora`) for this image using the {url-docker}[Docker client]:

 docs-site $ docker run -u $(id -u) -v $PWD:/antora:Z --rm -t antora/antora antora-playbook.yml

This command spins up a new container from the image, mounts the current directory as the path [.path]_/antora_ inside the container, runs the `antora` command (as the current user), then stops and removes the container.
It's exactly like running a locally installed `antora` command, only you're using container superpowers to do it!

Alternately, you can execute the `podman run` command to invoke the entrypoint command for this image using {url-podman}[podman]:

 docs-site $ podman run -v $PWD:/antora:Z --rm -t antora/antora antora-playbook.yml

The advantage of podman is that it's more secure.
It runs in user space and does not rely on a daemon.
To continue using podman, replace `docker` with `podman` (and drop the `-u` option) in any of the commands below.

=== Option flags

Here are explanations for some of the option flags used in the run command:

`-t`::
This flag allocates a pseudo-TTY, which is required if you want to see progress bars for git operations.
If you don't need to see these progress bars, you can omit this flag.

`-u $(id -u)`::
This option tells Docker to run the entrypoint command (i.e., `antora`) as the current user.
If you use the `:Z` modifier on the volume mount without specifying this option, the generated files are (most likely) written as the root user (and thus become rather tricky to delete).
This option is _not required_ when using podman.

`-v`::
A volume mount that maps the current directory on your local system (represented by `$PWD`) to the [.path]_/antora_ directory inside the container.
This allows files written by the container to be visible on your local system, which is the whole point of using the container.

`:Z` (on the volume mount)::
This flag is only required if you're running a Linux distribution that has SELinux enabled, such as Fedora.
This option allows you to use volume mounts when running SELinux.

CAUTION: Although tempting, the `--privileged` flag is not needed.
To learn more about using volume mounts with SELinux, see the blog post http://www.projectatomic.io/blog/2015/06/using-volumes-with-docker-can-cause-problems-with-selinux/[Using Volumes with Docker can Cause Problems with SELinux].

=== Cache directory location

If you want Antora to put the xref:cache.adoc[cache folder] inside the mounted directory, specify a playbook-relative cache directory in the command using the `--cache-dir` option:

 docs-site $ docker run -u $(id -u) -v $PWD:/antora:Z --rm -t antora/antora --cache-dir=./.cache/antora antora-playbook.yml

Now, all files cached or generated by Antora are neatly self-contained inside the mounted directory (and owned by the current user).

== Enter the container

If you want to shell into the container instead of having it run the `antora` command, append the name of the shell (`ash`) to the container run command:

 docs-site $ docker run -u $(id -u) -v $PWD:/antora:Z --rm -it antora/antora ash

Now you can run the `antora` command from anywhere inside the running container.
This mode is useful to use while editing.
Since the container continues to run, you can quickly execute the `antora` command.

If the base Antora image doesn't include everything you need for your site, you can extend it.

== Extend the Antora image

You can use this image as a base for your own Docker image.
The image comes preconfigured with Yarn so you can install additional extensions, such as {url-plantuml-npm}[Asciidoctor PlantUML] (`asciidoctor-plantuml`).

. Clone the docker-antora repository and switch to it:

 ~ $ git clone https://gitlab.com/antora/docker-antora.git && cd "$(basename $_ .git)"

. Create a custom Dockerfile file named [.path]_Dockerfile.custom_.
. Populate the file with the following contents:
+
.Dockerfile.custom
[source,docker]
----
FROM antora/antora

RUN yarn global add asciidoctor-plantuml # <1>
----
<1> Adds a custom extension to the base image.

. Build the image using the following command:

 docker-antora $ docker build -t local/antora:custom -f Dockerfile.custom .

Once the build is finished, you'll have a new image available on your machine named `local/antora:custom`.
To see a list of all your images, run the following command:

 $ docker images

To run this image, switch back to your playbook project and run the container as follows:

 docs-site $ docker run -u $(id -u) -v $PWD:/antora:Z --rm -t local/antora:custom antora-playbook.yml

If you want to share this image with others, you'll need to publish it.
Consult the {url-docker}[Docker documentation] to find out how.
